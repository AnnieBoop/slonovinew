import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { config } from '../config/config';
import 'rxjs/add/operator/map';

@Injectable()
export class SlonoviService {

    constructor(private http: Http) { }

    getAll() {
        return this.http
            .get(config.apiUrl + '/slonovi')
            .map((response) => { return response.json() });
    }

    getById(id) {
        return this.http
            .get(config.apiUrl + '/slonovi/' + id)
            .map((response) => { return response.json() });
    }

    post(slon) {
        return this.http
            .post(config.apiUrl + '/slonovi', slon)
            .map((response) => { return response.json() });
    }

    update(slon) {
        return this.http
            .patch(config.apiUrl + '/slonovi/' + slon.id, slon)
            .map((response) => { return response.json() });
    }

    delete(id) {
        return this.http
            .delete(config.apiUrl + '/slonovi/' + id)
            .map((response) => { return response.json() });
    }



}