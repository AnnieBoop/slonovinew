import { Component, OnInit } from '@angular/core';
import { SlonoviModel } from '../../models/slonovi.model';

@Component({
  selector: 'novi-slon',
  templateUrl: './novi-slon.route.html'
})
export class NoviSlonComponent implements OnInit {

  noviSlon = {
    ime: null,
    kilaza: null,
    visina: null,
    birthdate: {
      day: 1,
      month: 1,
      year: 2018
    }
  };

  constructor(public model: SlonoviModel) { }

  dodajSlona() {
    if (this.noviSlon.ime && this.noviSlon.kilaza && this.noviSlon.visina && this.noviSlon.birthdate) {
      this.model.dodajSlona(this.noviSlon);
    }
     console.log(this.noviSlon)
  }
  ngOnInit() {
  }

}
