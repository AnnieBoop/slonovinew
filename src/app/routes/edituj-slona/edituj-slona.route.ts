import { Component, OnInit } from '@angular/core';
import { SlonoviModel } from '../../models/slonovi.model';
import { config } from '../../config/config';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'edituj-slona',
  templateUrl: './edituj-slona.route.html'
})

export class EditujSlonaComponent implements OnInit {

  editovaniSlon = {};

  constructor(private route:ActivatedRoute,public model:SlonoviModel) {
    this.route.parent.params.subscribe(
      ({id}) => {
        this.model.getSlonaById(id,(slon)=>{
          this.editovaniSlon = slon;
        });
      }
    );
  }

  updejtujSlona(){
    this.model.updejtujSlona(this.editovaniSlon);
  }

  ngOnInit() {
  }

}