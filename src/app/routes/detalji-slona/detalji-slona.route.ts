import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SlonoviModel } from '../../models/slonovi.model';

@Component({
  selector: 'detalji-slona',
  templateUrl: './detalji-slona.route.html'
})
export class DetaljiSlonaComponent implements OnInit {

  slon = {};
  constructor(private route:ActivatedRoute,public model:SlonoviModel) {
    this.route.parent.params.subscribe(
      ({id}) => {
        this.model.getSlonaById(id,(slon)=>{
          this.slon = slon;
        });
      }
    );
  }

  ngOnInit() {

  }
}
