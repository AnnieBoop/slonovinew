import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class MenuModel {

 constructor(private router:Router){}

 linkovi = [
   {putanja:"/",naziv:"Kuca"},
   {putanja:"/dashboard",naziv:"Tabela slonova"},
   {putanja:"/slonovi/novi",naziv:"Dodaj slona"}
 ]

 goToPath(path){
   let pathArray = [];
   pathArray.push(path);
   this.router.navigate(pathArray);
 }

}

